# This is the configuration for Porter
# You must define steps for each action, but the rest is optional
# See https://porter.sh/author-bundles for documentation on how to configure your bundle
# Uncomment out the sections below to take full advantage of what Porter can do!

name: umami
version: "$CI_COMMIT_TAG"
description: "Umami is a simple, easy to use, self-hosted web analytics solution."
registry: "$DOCKER_REPO_PROJECT"

dockerfile: Dockerfile.tmpl

mixins:
  - exec
  - helm3-centros

custom:
  "one.centros.csm":
    "$schema": https://gitlab.com/centros.one/composer/schemas/-/raw/0.2.0/csm-schema.json
    sockets:
      input: []
      output: []

images:
  umami:
    description: "Umami service image"
    imageType: "docker"
    repository: ghcr.io/umami-software/umami
    tag: postgresql-v1.33.3
  postgresql:
    description: "Postgresql service image"
    imageType: "docker"
    repository: bitnami/postgresql
    tag: 14.4.0-debian-11-r9
  bitnami-shell:
    description: "Shell image"
    imageType: "docker"
    repository: bitnami/bitnami-shell
    tag: 11-debian-11-r14
  postgres-exporter:
    description: "Postgresql exporter image"
    imageType: "docker"
    repository: bitnami/postgres-exporter
    tag: 0.10.1-debian-11-r14

credentials:
  - name: kubeconfig
    path: /root/.kube/config

parameters:
  - name: values
    path: /root/helm.values
    properties:
      affinity:
        properties: {}
        type: object
      autoscaling:
        properties:
          enabled:
            type: boolean
          maxReplicas:
            type: integer
          minReplicas:
            type: integer
          targetCPUUtilizationPercentage:
            type: integer
          targetMemoryUtilizationPercentage:
            type: integer
        type: object
      extraEnv:
        properties: {}
        type: object
      fullnameOverride:
        type: string
      hashSalt:
        type: string
      image:
        properties:
          digest:
            type: string
          pullPolicy:
            type: string
          repository:
            type: string
        type: object
      imagePullSecrets:
        type: array
      ingress:
        properties:
          annotations:
            properties: {}
            type: object
          className:
            type: string
          enabled:
            type: boolean
          hosts:
            type: array
          tls:
            type: array
        type: object
      nameOverride:
        type: string
      nodeSelector:
        properties: {}
        type: object
      podAnnotations:
        properties: {}
        type: object
      podSecurityContext:
        properties: {}
        type: object
      postgresql:
        properties:
          auth:
            properties:
              database:
                type: string
              password:
                type: string
              postgresPassword:
                type: string
              username:
                type: string
            required:
              - postgresPassword
              - password
            type: object
          primary:
            properties:
              primary:
                properties:
                  affinity:
                    properties: {}
                    type: object
                  annotations:
                    properties: {}
                    type: object
                  args:
                    type: array
                  command:
                    type: array
                  configuration:
                    type: string
                  containerSecurityContext:
                    properties:
                      enabled:
                        type: boolean
                      runAsUser:
                        type: integer
                    type: object
                  customLivenessProbe:
                    properties: {}
                    type: object
                  customReadinessProbe:
                    properties: {}
                    type: object
                  customStartupProbe:
                    properties: {}
                    type: object
                  existingConfigmap:
                    type: string
                  existingExtendedConfigmap:
                    type: string
                  extendedConfiguration:
                    type: string
                  extraEnvVars:
                    type: array
                  extraEnvVarsCM:
                    type: string
                  extraEnvVarsSecret:
                    type: string
                  extraPodSpec:
                    properties: {}
                    type: object
                  extraVolumeMounts:
                    type: array
                  extraVolumes:
                    type: array
                  hostAliases:
                    type: array
                  hostIPC:
                    type: boolean
                  hostNetwork:
                    type: boolean
                  initContainers:
                    type: array
                  initdb:
                    properties:
                      args:
                        type: string
                      password:
                        type: string
                      postgresqlWalDir:
                        type: string
                      scripts:
                        properties: {}
                        type: object
                      scriptsConfigMap:
                        type: string
                      scriptsSecret:
                        type: string
                      user:
                        type: string
                    type: object
                  labels:
                    properties: {}
                    type: object
                  lifecycleHooks:
                    properties: {}
                    type: object
                  livenessProbe:
                    properties:
                      enabled:
                        type: boolean
                      failureThreshold:
                        type: integer
                      initialDelaySeconds:
                        type: integer
                      periodSeconds:
                        type: integer
                      successThreshold:
                        type: integer
                      timeoutSeconds:
                        type: integer
                    type: object
                  nodeAffinityPreset:
                    properties:
                      key:
                        type: string
                      type:
                        type: string
                      values:
                        type: array
                    type: object
                  nodeSelector:
                    properties: {}
                    type: object
                  persistence:
                    properties:
                      accessModes:
                        type: array
                      annotations:
                        properties: {}
                        type: object
                      dataSource:
                        properties: {}
                        type: object
                      enabled:
                        type: boolean
                      existingClaim:
                        type: string
                      mountPath:
                        type: string
                      selector:
                        properties: {}
                        type: object
                      size:
                        type: string
                      storageClass:
                        type: string
                      subPath:
                        type: string
                    type: object
                  pgHbaConfiguration:
                    type: string
                  podAffinityPreset:
                    type: string
                  podAnnotations:
                    properties: {}
                    type: object
                  podAntiAffinityPreset:
                    type: string
                  podLabels:
                    properties: {}
                    type: object
                  podSecurityContext:
                    properties:
                      enabled:
                        type: boolean
                      fsGroup:
                        type: integer
                    type: object
                  priorityClassName:
                    type: string
                  readinessProbe:
                    properties:
                      enabled:
                        type: boolean
                      failureThreshold:
                        type: integer
                      initialDelaySeconds:
                        type: integer
                      periodSeconds:
                        type: integer
                      successThreshold:
                        type: integer
                      timeoutSeconds:
                        type: integer
                    type: object
                  resources:
                    properties:
                      limits:
                        properties: {}
                        type: object
                      requests:
                        properties: {}
                        type: object
                    type: object
                  schedulerName:
                    type: string
                  service:
                    properties:
                      annotations:
                        properties: {}
                        type: object
                      clusterIP:
                        type: string
                      externalTrafficPolicy:
                        type: string
                      extraPorts:
                        type: array
                      loadBalancerIP:
                        type: string
                      loadBalancerSourceRanges:
                        type: array
                      nodePorts:
                        properties:
                          postgresql:
                            type: string
                        type: object
                      ports:
                        properties:
                          postgresql:
                            type: integer
                        type: object
                      sessionAffinity:
                        type: string
                      sessionAffinityConfig:
                        properties: {}
                        type: object
                      type:
                        type: string
                    type: object
                  sidecars:
                    type: array
                  standby:
                    properties:
                      enabled:
                        type: boolean
                      primaryHost:
                        type: string
                      primaryPort:
                        type: string
                    type: object
                  startupProbe:
                    properties:
                      enabled:
                        type: boolean
                      failureThreshold:
                        type: integer
                      initialDelaySeconds:
                        type: integer
                      periodSeconds:
                        type: integer
                      successThreshold:
                        type: integer
                      timeoutSeconds:
                        type: integer
                    type: object
                  terminationGracePeriodSeconds:
                    type: string
                  tolerations:
                    type: array
                  topologySpreadConstraints:
                    type: array
                  updateStrategy:
                    properties:
                      rollingUpdate:
                        properties: {}
                        type: object
                      type:
                        type: string
                    type: object
                type: object
            type: object
        required:
          - auth
        type: object
      replicaCount:
        type: integer
      resources:
        properties: {}
        type: object
      securityContext:
        properties: {}
        type: object
      service:
        properties:
          port:
            type: integer
          type:
            type: string
        type: object
      serviceAccount:
        properties:
          annotations:
            properties: {}
            type: object
          create:
            type: boolean
          name:
            type: string
        type: object
      tolerations:
        type: array
    required:
      - hashSalt
      - postgresql
    type: object
    applyTo:
      - install
      - upgrade
  - name: image-pull-secret
    type: string
    default: ""
    env: IMAGE_PULL_SECRET
    applyTo:
      - install
      - upgrade
  - name: namespace
    type: string
    default: default

install:
  - exec:
      description: "Prepare Helm chart"
      command: ./helpers.sh
      arguments:
        - prepare
        - ./helm
        - '"{{ bundle.name }}"'
        - '"{{ bundle.description }}"'
        - '"{{ bundle.version }}"'
  - exec:
      description: "Write image override values file"
      command: ./helpers.sh
      arguments:
        - write-values
        - /root/image.values
        - "image.digest"
        - '"{{ bundle.images.umami.digest }}"'
        - "image.repository"
        - '"{{ bundle.images.umami.repository }}"'
        - "postgresql.image.tag"
        - '"{{ bundle.images.postgresql.digest }}"'
        - "postgresql.image.repository"
        - '"{{ bundle.images.postgresql.repository }}"'
        - "postgresql.volumePermissions.image.tag"
        - '"{{ bundle.images.bitnami-shell.digest }}"'
        - "postgresql.volumePermissions.image.repository"
        - '"{{ bundle.images.bitnami-shell.repository }}"'
        - "postgresql.metrics.image.tag"
        - '"{{ bundle.images.postgres-exporter.digest }}"'
        - "postgresql.metrics.image.repository"
        - '"{{ bundle.images.postgres-exporter.repository }}"'
        - "imagePullSecrets[0].name"
        - '"{{ bundle.parameters.image-pull-secret }}"'
        - "postgresql.global.imagePullSecrets[0]"
        - '"{{ bundle.parameters.image-pull-secret }}"'
  - helm3-centros:
      description: "Install {{ bundle.name }}"
      name: "{{ installation.name }}"
      chart: "./helm"
      namespace: "{{ bundle.parameters.namespace }}"
      wait: true
      upsert: true
      idempotent: true
      values:
        - /root/image.values
        - /root/helm.values

upgrade:
  - exec:
      description: "Prepare Helm chart"
      command: ./helpers.sh
      arguments:
        - prepare
        - ./helm
        - '"{{ bundle.name }}"'
        - '"{{ bundle.description }}"'
        - '"{{ bundle.version }}"'
  - exec:
      description: "Write image override values file"
      command: ./helpers.sh
      arguments:
        - write-values
        - /root/image.values
        - "image.digest"
        - '"{{ bundle.images.umami.digest }}"'
        - "image.repository"
        - '"{{ bundle.images.umami.repository }}"'
        - "postgresql.image.tag"
        - '"{{ bundle.images.postgresql.digest }}"'
        - "postgresql.image.repository"
        - '"{{ bundle.images.postgresql.repository }}"'
        - "postgresql.volumePermissions.image.tag"
        - '"{{ bundle.images.bitnami-shell.digest }}"'
        - "postgresql.volumePermissions.image.repository"
        - '"{{ bundle.images.bitnami-shell.repository }}"'
        - "postgresql.metrics.image.tag"
        - '"{{ bundle.images.postgres-exporter.digest }}"'
        - "postgresql.metrics.image.repository"
        - '"{{ bundle.images.postgres-exporter.repository }}"'
        - "imagePullSecrets[0].name"
        - '"{{ bundle.parameters.image-pull-secret }}"'
        - "postgresql.global.imagePullSecrets[0]"
        - '"{{ bundle.parameters.image-pull-secret }}"'
  - helm3-centros:
      description: "Upgrade {{ bundle.name }}"
      name: "{{ installation.name }}"
      chart: "./helm"
      namespace: "{{ bundle.parameters.namespace }}"
      wait: true
      resetValues: true
      reuseValues: false
      values:
        - /root/image.values
        - /root/helm.values

uninstall:
  - helm3-centros:
      description: "Uninstall {{ bundle.name }}"
      namespace: "{{ bundle.parameters.namespace }}"
      releases:
        - "{{ installation.name }}"
